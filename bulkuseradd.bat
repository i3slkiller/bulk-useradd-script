@echo off
REM Bulk useradd script for Windows NT 4.0+
REM (C) 2023 i3slkiller 
REM 
REM This script use these external binaries:
REM * SQLite - REQUIRED, used for adding users and groups based on data from SQLite database
REM * ImageMagick - for resizing avatars from SQLite database
REM * SetACL - for setting permissions for registry keys
REM * setUserAvatar - for setting user avatars on Windows Vista+, code copied from http://web.archive.org/web/20141219050256/http://joco.name/2010/12/06/i-discovered-the-new-windows-user-tile-api/
REM On Windows 2000 and older:
REM * reg.exe - from Windows XP, for reading registry values
REM On Windows NT 4.0
REM * cmd.exe - from Windows 2000, for executing this script
REM
REM On Windows NT 4.0 to execute this script type:
REM > bin\x86\nt4\cmd.exe /c bulkuseradd.bat

setlocal enabledelayedexpansion

REM check admin privileges
net session > nul 2> nul
if not %errorlevel% equ 0 echo This script require admin privileges && exit /b 1

REM Get absolute path to folder inwhere is this script
set scriptdir=%~dp0
set scriptdir=%scriptdir:~0,-1%

REM Check if reg.exe exists on system. If not, use bundled reg.exe from Windows XP (on 2000 & NT 4.0)
reg /? > nul 2> nul
if %errorlevel% equ 0 (
	set regbin=reg
) else (
	set regbin=%scriptdir%\bin\%PROCESSOR_ARCHITECTURE%\2k\reg.exe
)

REM Check Windows NT Version
call :checkwinver

REM Initialize default parameter variables
set LIST_GROUPS=0
set LIST_USERS=0
set SET_AVATAR=1

REM Set default database file
set database=users.db

REM Set Unicode codepage on Windows 7+ (on Vista and older it just stops script)
if %ntver% geq %win7% (
	for /f "tokens=3" %%f in ('^""%regbin%" query "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Nls\CodePage" /v "OEMCP" ^| findstr REG_SZ^"') do set defaultcp=%%f
	@chcp 65001 > nul
)

REM Parse command-line arguments
:parse
if "%~1"=="" goto :endparse
if "%~1"=="-db" set "database=%~2" && shift
if "%~1"=="--database" set "database=%~2" && shift
if "%~1"=="-lg" set LIST_GROUPS=1
if "%~1"=="--list-groups" set LIST_GROUPS=1
if "%~1"=="-lu" set LIST_USERS=1
if "%~1"=="--list-users" set LIST_USERS=1
if "%~1"=="-na" set SET_AVATAR=0
if "%~1"=="--no-avatars" set SET_AVATAR=0
if "%~1"=="-h" goto :help
if "%~1"=="--help" goto :help
shift
goto :parse
:endparse

REM Set SQLite executable variable
set sqlitebin=%scriptdir%\bin\sqlite3.exe

REM Stop if database don't exists
if not exist "%database%" echo Database "%database%" not found && set exitcode=2 && goto :end

REM Stop if SQLite executable not exists
if not exist "%sqlitebin%" echo SQLite executable not found && set exitcode=3 && goto :end

REM List groups or users if suitable command-line argument is entered
if %LIST_GROUPS% equ 1 goto :listgroups
if %LIST_USERS% equ 1 goto :listusers

REM Set image-processing and modifying registry ACL executables into variables
if %SET_AVATAR% equ 1 if %ntver% geq %winxp% if %ntver% leq %win2003% (
	set imagickbin=%scriptdir%\bin\%PROCESSOR_ARCHITECTURE%\xp\magick.exe
	if not exist "!imagickbin!" set SET_AVATAR=0
)
if %SET_AVATAR% equ 1 if %ntver% geq %winvista% (
	set setavatarbin=%scriptdir%\bin\setUserAvatar.exe
	if not exist "!setavatarbin!" set SET_AVATAR=0
)
if %SET_AVATAR% equ 1 if %ntver% geq %win8% (
	set imagickbin=%scriptdir%\bin\%PROCESSOR_ARCHITECTURE%\magick.exe
	if not exist "!imagickbin!" set SET_AVATAR=0
)
if %SET_AVATAR% equ 1 if %ntver% geq %win8% (
	set setaclbin=%scriptdir%\bin\%PROCESSOR_ARCHITECTURE%\setacl.exe
	if not exist "!setaclbin!" echo Warning: SetACL executable not found. User avatar may display fine, but registry key permissions may be not correct.
)

if %SET_AVATAR% equ 1 if %ntver% geq %win8% (
	REM Check if wmic command is available
	wmic /? > nul 2> nul
	if !errorlevel! equ 0 (
		set WMIC_AVAILABLE=1
	) else (
		set WMIC_AVAILABLE=0
	)
	
	REM Check Administrator account SID if Windows version is 8 or newer (it is required for set permissions on avatar files and registry keys)
	if !WMIC_AVAILABLE! equ 1 (
		for /f "usebackq" %%f in (`wmic useraccount where "sid like 'S-1-5-%%%%-500'" get sid ^| findstr S-`) do set adminsid=%%f
	) else (
		for /f "usebackq" %%f in (`powershell -c "Get-CimInstance -ClassName Win32_UserAccount -Filter \"sid like 'S-1-5-%%%%-500'\" | Select SID" ^| findstr S-`) do set adminsid=%%f
	)
)

REM Create tempdir
set tempdir=%TEMP%\bulkuseradd-%RANDOM%
mkdir "%tempdir%"

REM Add groups and users
call :addgroups
call :addusers

REM Cleanup
rd /s /q "%tempdir%"

goto :end
REM ======== END OF SCRIPT ========

REM ======== BEGIN FUNCTIONS ========
REM begin check windows version
:checkwinver
for /f "tokens=3" %%f in ('^""%regbin%" query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v "CurrentVersion" ^| findstr REG_SZ^"') do (
	for /f "tokens=1-2 delims=." %%g in ('echo %%f') do set ntver=%%g0%%h
)

if %ntver% equ 603 (
	"%regbin%" query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v "CurrentMajorVersionNumber" > nul 2> nul && (
		for /f "tokens=3" %%f in ('^""%regbin%" query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v "CurrentMajorVersionNumber" ^| findstr REG_DWORD^"') do set /a major=%%f > nul
		for /f "tokens=3" %%f in ('^""%regbin%" query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v "CurrentMinorVersionNumber" ^| findstr REG_DWORD^"') do set /a minor=%%f > nul
		set ntver=!major!0!minor!
	)
)

set winnt4=400
set win2k=500
set winxp=501
set win2003=502
set winvista=600
set win7=601
set win8=602
set win81=603
set win10=1000

for /f "tokens=3" %%f in ('^""%regbin%" query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v "CurrentBuildNumber" ^| findstr REG_SZ^"') do set winbuild=%%f
goto :eof
REM end check windows version

REM begin list groups
:listgroups
"%sqlitebin%" -table "%database%" "SELECT groupname, description FROM groups"
goto :end
REM end list groups

REM begin list users
:listusers
"%sqlitebin%" -table "%database%" "SELECT username, groupname, displayname, password, length(avatar), description, additionalinfo FROM users"
goto :end
REM end list users

REM begin create groups
:addgroups
for /f "tokens=1-2 delims=|" %%i in ('^""%sqlitebin%" -nullvalue null "%database%" "SELECT groupname, description FROM groups"^"') do (
	net localgroup "%%i" > nul 2> nul
	if !errorlevel! equ 2 (
		set uacmd=net localgroup "%%i"
		if not %%j==null set uacmd=!uacmd! /comment:"%%j"
		set uacmd=!uacmd! /add
		
		!uacmd! > nul 2> nul
		if !errorlevel! equ 0 ( echo Group "%%i" added ) else ( echo Failed to add group "%%i" )
	) else if !errorlevel! equ 0 ( echo Group "%%i" exists ) else ( echo Invalid groupname "%%i" )
)
goto :eof
REM end create groups

REM begin create users
:addusers
for /f "tokens=1-6 delims=|" %%i in ('^""%sqlitebin%" -nullvalue null "%database%" "SELECT username, password, displayname, description, groupname, length(avatar) FROM users"^"') do (
	net user "%%i" > nul 2> nul
	if !errorlevel! equ 2 (
		REM create user
		set uacmd=net user "%%i"
		if not %%j==null set uacmd=!uacmd! "%%j"
		if not %%k==null set uacmd=!uacmd! /fullname:"%%k"
		if not %%l==null set uacmd=!uacmd! /comment:"%%l"
		set uacmd=!uacmd! /add
		
		!uacmd! > nul 2> nul
		if !errorlevel! equ 0 (
			echo User "%%i" added
		
			REM assign user to group
			if not %%m==null (
				net localgroup "%%m" "%%i" /add > nul 2> nul
				if !errorlevel! equ 0 ( echo User "%%i" assigned to group "%%m" ) else ( echo Failed to assign user "%%i" to group "%%m" )
			)
			
			REM set user avatar
			if !SET_AVATAR! equ 1 if !ntver! geq !winxp! if not %%n==null if not %%n equ 0 (
				"!sqlitebin!" "!database!" "SELECT writefile('!tempdir!\img', avatar) FROM users WHERE username='%%i'" > nul
				
				if !ntver! geq !winxp! if !ntver! leq !win2003! (
					for /f "tokens=3,*" %%f in ('^""!regbin!" query "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Common AppData" ^| findstr REG_SZ^"') do (
						"!imagickbin!" "!tempdir!\img" -thumbnail 48x48^^^^ -gravity center -extent 48x48 BMP3:"%%g\Microsoft\User Account Pictures\%%i.bmp"
					)
				)
				if !ntver! geq !winvista! "!setavatarbin!" "%%i" "!tempdir!\img"
				
				if !ntver! geq !win8! (
					if !WMIC_AVAILABLE! equ 1 (
						for /f "usebackq" %%f in (`wmic useraccount where "name='%%i'" get sid ^| findstr S-`) do set sid=%%f
					) else (
						REM Saving user SID to file is workaround, because I can't force to get output of this command to variable in any way
						powershell -c "Get-CimInstance -ClassName Win32_UserAccount -Filter \"name='%%i'\" | Select SID" | findstr S- > "!tempdir!\sid"
						for /f "usebackq" %%f in (`type "!tempdir!\sid"`) do set sid=%%f
						del "!tempdir!\sid"
					)
				
					if not exist "%PUBLIC%\AccountPictures" (
						mkdir "%PUBLIC%\AccountPictures"
						attrib +h +r "%PUBLIC%\AccountPictures" > nul
						icacls "%PUBLIC%\AccountPictures" /setowner "*S-1-5-18" > nul
						icacls "%PUBLIC%\AccountPictures" /inheritance:e > nul
					)
					set out=%PUBLIC%\AccountPictures\!sid!
					mkdir "!out!"
					
					set regout=HKLM\Software\Microsoft\Windows\CurrentVersion\AccountPicture\Users\!sid!
					"!regbin!" add "!regout!" > nul
					
					REM In Windows 8.1 avatar sizes are 40, 96, 200, 240 and 448.
					REM In Windows 10 up to 1909 (build 18363) there are 32, 40, 48, 96, 192, 240, 448,
					REM but in Windows 10 2004+ (build 19041) and 11 there are 32, 40, 48, 64, 96, 192, 208, 240, 424, 448 and 1080.
					if !ntver! geq !win10! (
						if !winbuild! geq 19041 (
							set avatarsizes=32 40 48 64 96 192 208 240 424 448 1080
						) else (
							set avatarsizes=32 40 48 96 192 240 448
						)
					) else (
						set avatarsizes=40 96 200 240 448
					)
					
					set imcmd="!imagickbin!" "!tempdir!\img" -background "#FFFFFF" -write mpr:av
					for %%f in (!avatarsizes!) do (
						set imcmd=!imcmd! ^( mpr:av -thumbnail %%fx%%f^^^^ -gravity center -extent %%fx%%f +write "!out!\Image%%f.jpg" ^)
						"!regbin!" add "!regout!" /v Image%%f /t REG_SZ /d "!out!\Image%%f.jpg" > nul
					)
					set imcmd=!imcmd! null:
					!imcmd! > nul
					
					REM attrib -a cant remove archive attribute, I don't know why
					attrib -a +s +h +i "!out!\*" > nul
					attrib +r "!out!" > nul
					icacls "!out!" /inheritance:r /grant:r "*S-1-5-18":"(OI)(CI)F" > nul
					icacls "!out!" /grant "*!adminsid!":"(OI)(CI)F" > nul
					icacls "!out!" /grant "*S-1-1-0":"(OI)(CI)R" > nul
					icacls "!out!" /grant "*S-1-5-32-544":"(OI)(CI)F" > nul
					icacls "!out!" /setowner "*S-1-5-18" /t > nul
					icacls "!out!" /remove:g "*S-1-5-32-544" > nul
					
					if exist "!setaclbin!" (
						"!setaclbin!" -on "!regout!" -ot reg -actn setprot -op "dacl:p_nc;sacl:p_nc" > nul
						"!setaclbin!" -on "!regout!" -ot reg -actn clear -clr dacl,sacl > nul
						"!setaclbin!" -on "!regout!" -ot reg -actn ace -ace "n:S-1-5-18;p:full;i:sc,so" > nul
						"!setaclbin!" -on "!regout!" -ot reg -actn ace -ace "n:!adminsid!;p:full;i:sc,so" > nul
						"!setaclbin!" -on "!regout!" -ot reg -actn ace -ace "n:S-1-1-0;p:read;i:sc,so" > nul
						if !ntver! geq !win10! "!setaclbin!" -on "!regout!" -ot reg -actn setowner -ownr "n:S-1-5-18" > nul
					)
				)
				
				del "!tempdir!\img"
				
				echo Avatar for user "%%i" was set
			)
		) else ( echo Failed to add user "%%i" )
	) else if !errorlevel! equ 0 ( echo User "%%i" exists ) else ( echo Invalid username "%%i" )
)
goto :eof
REM end create users

REM begin help
:help
echo Usage: %~0 [arguments]
echo.
echo Arguments:
echo -h, --help                    print help
echo -db, --database DATABASE.db   read data from selected SQLite database
echo                               (default: users.db)
echo -na, --no-avatars             don't set user avatars
echo -lg, --list-groups            list groups
echo -lu, --list-users             list users
goto :end
REM end help

:end
if %ntver% geq %win7% @chcp %defaultcp% > nul
if not defined exitcode set exitcode=0
exit /b %exitcode%
