#!/system/bin/sh
# Bulk useradd script for Android
# (C) 2023 i3slkiller

sqlitebin=${sqlitebin:-sqlite3}
database=${database:-users.db}

#if [ $USER_ID != 0 ]; then echo "This script require root privileges"; exit 1; fi

function listusers {
	$sqlitebin $database 'SELECT username FROM users'
}

function addusers {
	$sqlitebin $database 'SELECT username FROM users' > "$tempdir/users"
	while IFS="|" read -r username; do
		pm list users | grep ":$username:" > /dev/null
		if [ $? != 0 ]; then
			if pm create-user "$username" > /dev/null; then echo "User \"$username\" added"; fi
		else
			echo "User \"$username\" exists"
		fi
	done < "$tempdir/users"
}

function bulkuseraddhelp {
	echo "Usage: $0 [arguments]"
	echo ""
	echo "Arguments:"
	echo "-h, --help                    print help"
	echo "-db, --database DATABASE.db   read data from selected SQLite database (default: users.db)"
	echo "-lu, --list-users             list users"
}

LIST_USERS=0

for i in "$@"; do
	case $i in
		-db|--database)
			database="$2"
			shift 2
			;;
		-lu|--list-users)
			LIST_USERS=1
			shift
			;;
		-h|--help)
			bulkuseraddhelp
			exit
			;;
		-*|--*)
			echo "Unknown option $i"
			exit 1
			;;
		*)
			;;
	esac
done

if [ ! -f "$database" ]; then echo "Database \"$database\" not found"; exit 2; fi
if [ ! "$($sqlitebin --version 2> /dev/null)" ]; then echo SQLite executable not found; exit 3; fi

if [ $LIST_USERS == 1 ]; then listusers; exit; fi

tempdir="/data/local/tmp/bulkuseradd-$(date +%Y%m%d%H%M%S%N)"
mkdir -m 0700 "$tempdir"

addusers

rm -rf "$tempdir"
