# Bulk useradd script
This script create users based on data from SQLite database.

## Structure
* `bin/` - Windows executables
	* `AMD64/` - Executables for 64-bit Windows
		* `xp/` - Executables working on Windows XP
	* `x86/` - Executables for 32-bit Windows
		* `2k/` - Executables working on Windows 2000 and NT 4.0
		* `nt4/` - Executables working on Windows NT 4.0
		* `xp/` - Executables working on Windows XP
* `src/` - Source code for some tools
* `bulkuseradd.bat` - script for Windows
* `bulkuseradd.sh` - script for Linux
* `bulkuseradd-android.sh` - script for Android (incomplete)
* `struktura.sql` - SQLite database structure
* `users.db` - SQLite database containing groups, users and related data

## Usage
### On Windows 2000+
```bat
bulkuseradd.bat [-db|--database users.db] [-lg|--list-groups] [-lu|--list-users] [-na|--no-avatars]
```

### On Windows NT 4.0
On Windows NT 4.0 this script must be runned in cmd.exe from Windows 2000
```bat
bin\x86\nt4\cmd.exe /c "bulkuseradd.bat [-db|--database users.db] [-lg|--list-groups] [-lu|--list-users] [-na|--no-avatars]"
```

### Linux
```sh
./bulkuseradd.sh [-db|--database users.db] [-lg|--list-groups] [-lu|--list-users] [-na|--no-avatars] [-anp|--allow-no-passwd]
```
or

```sh
bash bulkuseradd.sh [-db|--database users.db] [-lg|--list-groups] [-lu|--list-users] [-na|--no-avatars] [-anp|--allow-no-passwd]
```

## How this script work?
* set executable commands and database path variables
* check if executables works and database exist
* create groups
* when creating users
	* add user account
	* assign him to group¹
	* set password²
	* set avatar³
* create home directories (macOS only)½

¹ - user is assigned to group during adding user on Unix-like systems  
² - password is set during create user in NetBSD, OpenBSD and Windows  
³ - only if ImageMagick executable exists (not applicable to macOS) and in case of Windows in XP and newer (in Vista+ setUserAvatar.exe is also required)  
½ - home directory is created during adding user (Unix-like expect macOS) or during first login (Windows)  

## Data used from database on various systems
### Groups

| OS/column | groupname | displayname | description |
| --------- | --------- | ----------- | ----------- |
| Windows   | used |  | used |
| Linux     | used |  |  |
| FreeBSD   | used |  |  |
| OpenBSD   | used |  |  |
| NetBSD    | used |  |  |
| macOS     | used | used | used |
| Android   |  |  |  |

### Users

| OS/column | username | groupname | displayname | password | avatar | description | additionalInfo |
| --------- | -------- | --------- | ----------- | -------- | ------ | ----------- | -------------- |
| Windows   | used | used | used | used | used | used |  |
| Linux     | used | used | used | used | used |  |  |
| FreeBSD   | used | used | used | used | used |  |  |
| OpenBSD   | used | used | used | used | used |  |  |
| NetBSD    | used | used | used | used | used |  |  |
| macOS     | used | used | used | used | used | used |  |
| Android   | used |  |  |  |  |  |  |