CREATE TABLE IF NOT EXISTS "groups" (
	"groupname"	TEXT NOT NULL CHECK(TRIM(groupname)<>'') UNIQUE,
	"displayname"	TEXT,
	"description"	TEXT
);
CREATE TABLE IF NOT EXISTS "users" (
	"username"	TEXT NOT NULL CHECK(TRIM(username)<>'') UNIQUE,
	"groupname"	TEXT,
	"displayname"	TEXT,
	"password"	TEXT,
	"avatar"	BLOB,
	"description"	TEXT,
	"additionalinfo"	TEXT,
	FOREIGN KEY("groupname") REFERENCES "groups"("groupname")
);
CREATE TRIGGER addGroupsOnInsert BEFORE INSERT ON users BEGIN
	INSERT OR IGNORE INTO groups (groupname) VALUES (NEW.groupname);
END;
CREATE TRIGGER addGroupsOnUpdate BEFORE UPDATE ON users BEGIN
	INSERT OR IGNORE INTO groups (groupname) VALUES (NEW.groupname);
END;
