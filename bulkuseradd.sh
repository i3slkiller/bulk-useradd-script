#!/bin/bash
# Bulk useradd script for Linux, and possibly other UNIX-like systems
# (C) 2023 i3slkiller
#
# This script use these external binaries:
# * SQLite - REQUIRED, used for adding users and groups based on data from SQLite database
# * ImageMagick - for resizing avatars from SQLite database

if [ $UID != 0 ]; then echo "This script require root privileges"; exit 1; fi

sqlitebin=${sqlitebin:-sqlite3}
imagickbin=${imagickbin:-convert}
database=${database:-users.db}
useraddargs="-m"

kernel=$(uname -s)

if [ $kernel == "FreeBSD" ]; then
	groupadd="pw group add"
	useradd="pw user add"
elif [ $kernel == "Darwin" ]; then
	groupadd="dseditgroup -o create"
else
	groupadd="groupadd"
	useradd="useradd"
fi

if [ $kernel == "OpenBSD" ]; then
	pwdcrypt="encrypt"
elif [ $kernel == "NetBSD" ]; then
	pwdcrypt="pwhash"
fi

function listgroups {
	$sqlitebin -table $database 'SELECT groupname, displayname, description FROM groups'
}

function listusers {
	$sqlitebin -table $database 'SELECT username, groupname, displayname, password, length(avatar), description, additionalinfo FROM users'
}

function addgroups {
	while IFS="|" read -r -a row; do
		if [ $kernel == "Darwin" ]; then
			groupcheck="dscl . read /Groups/'${row[0]}'"
		else
			groupcheck="getent group '${row[0]}'"
		fi
		
		if eval $groupcheck &> /dev/null; then
			echo "Group \"${row[0]}\" exists"
		else
			groupaddcmd="$groupadd"
			if [ $kernel == "Darwin" ]; then
				if [ ! -z "${row[1]}" ]; then groupaddcmd+=" -r '${row[1]}'"; fi
				if [ ! -z "${row[2]}" ]; then groupaddcmd+=" -c '${row[2]}'"; fi
			fi
			groupaddcmd+=" '${row[0]}'"
			
			if eval $groupaddcmd 2> /dev/null; then echo "Group \"${row[0]}\" added"; else echo "Failed to add group \"${row[0]}\""; fi
		fi
	done < <($sqlitebin $database 'SELECT groupname, displayname, description FROM groups')
}

function addusers {
	while IFS="|" read -r -a row; do
		if [ $kernel == "Darwin" ]; then
			usercheck="dscl . read /Users/'${row[0]}'"
		else
			usercheck="getent passwd '${row[0]}'"
		fi
		
		if eval $usercheck &> /dev/null; then
			echo "User \"${row[0]}\" exists"
		else
			if [ $kernel == "Darwin" ]; then
				_username="${row[0]}"
				[ -z "${row[2]}" ] && _displayname="${row[0]}" || _displayname="${row[2]}"
				[ -z "${row[5]}" ] && _descriptioncmd="true" || _descriptioncmd="dscl . create /Users/\"$_username\" Comment \"${row[5]}\""
				useraddcmd="_emejzingaddhipster"
			else
				useraddcmd="$useradd"
				if [ $kernel != "OpenBSD" ] && [ $kernel != "NetBSD" ]; then useraddcmd+=" '${row[0]}'"; fi
				useraddcmd+=" $useraddargs"
				if [ ! -z "${row[1]}" ]; then useraddcmd+=" -g '${row[1]}'"; else useraddcmd+=" -g 'nogroup'"; fi
				if [ ! -z "${row[2]}" ]; then useraddcmd+=" -c '${row[2]}'"; fi
				if [ $kernel == "OpenBSD" ] || [ $kernel == "NetBSD" ]; then
					if [ ! -z "${row[3]}" ]; then useraddcmd+=" -p '$($pwdcrypt "${row[3]}")'" ; fi
					useraddcmd+=" '${row[0]}'"
				fi
			fi
			
			if eval $useraddcmd 2> /dev/null; then
				echo "User \"${row[0]}\" added"
				
				if [ $kernel == "Darwin" ]; then
					 if [ ! -z "${row[1]}" ]; then
						if dseditgroup -o edit -a "${row[0]}" -t user "${row[1]}"; then
							echo "User \"${row[0]}\" assigned to group \"${row[1]}\""
						else
							echo "Failed to assign user \"${row[0]}\" to group \"${row[1]}\""
						fi
					fi
				fi
				
				if [ $kernel != "OpenBSD" ] && [ $kernel != "NetBSD" ]; then
					if [ ! -z "${row[3]}" ]; then
						if [ $kernel == "FreeBSD" ]; then
							passwdcmd="echo \"${row[3]}\" | pw user mod \"${row[0]}\" -h 0"
						elif [ $kernel == "Darwin" ]; then
							passwdcmd="dscl . passwd /Users/\"${row[0]}\" \"${row[3]}\""
						else
							passwdcmd="echo -e \"${row[3]}\n${row[3]}\n\" | passwd \"${row[0]}\""
						fi
						
						if eval $passwdcmd &> /dev/null; then
							echo "Password for user \"${row[0]}\" was set"
						else
							echo "Failed to set password for user \"${row[0]}\""
						fi
					elif [ $ALLOW_NO_PASSWD == 1 ]; then
						if [ $kernel == "FreeBSD" ]; then
							nopasswdcmd="pw user mod \"${row[0]}\" -w none"
						elif [ $kernel == "Darwin" ]; then
							nopasswdcmd="dscl . passwd /Users/\"${row[0]}\" \"\""
						else
							nopasswdcmd="passwd -d \"${row[0]}\""
						fi
						
						if eval $nopasswdcmd &> /dev/null; then
							echo "User \"${row[0]}\" allowed to login without password"
						else
							echo "Falied to allow user \"${row[0]}\" to login without password"
						fi
					fi
				fi
				
				if [ $SET_AVATAR == 1 ] && [ ! -z ${row[4]} ] && [ ${row[4]} != 0 ]; then
					$sqlitebin $database "SELECT writefile('$tempdir/img', avatar) FROM users WHERE username='${row[0]}'" > /dev/null
					
					if [ $kernel == "Darwin" ]; then
						mv "$tempdir/img" "/Library/User Pictures/${row[0]}"
						dscl . create /Users/"${row[0]}" Picture "/Library/User Pictures/${row[0]}"
					else
						$imagickbin "$tempdir/img" -gravity center -crop 1:1 "$tempdir/face.png"
						eval cp "$tempdir/face.png" "~${row[0]}/.face"
						eval chown "${row[0]}": "~${row[0]}/.face"
						if [ -d /var/lib/AccountsService/icons ] && [ -d /var/lib/AccountsService/users ]; then 
							cp "$tempdir/face.png" "/var/lib/AccountsService/icons/${row[0]}"
							echo -e "[User]\nIcon=/var/lib/AccountsService/icons/${row[0]}" > "/var/lib/AccountsService/users/${row[0]}"
						fi
						rm "$tempdir/"*
					fi
					
					echo "Avatar for user \"${row[0]}\" was set"
				fi
			else echo "Failed to add user \"${row[0]}\""; fi
		fi
	done < <($sqlitebin $database 'SELECT username, groupname, displayname, password, length(avatar), description FROM users')
}

function _emejzingaddhipster {
	lastUID=$(dscl . -list /Users UniqueID | awk '{print $2}' | sort -n | tail -1)
	nextUID=$(($lastUID + 1))
	
	{
		dscl . create /Users/"$_username" UniqueID $nextUID &&
		dscl . create /Users/"$_username" PrimaryGroupID 20 &&
		dscl . create /Users/"$_username" RealName "$_displayname" &&
		eval $_descriptioncmd &&
		dscl . create /Users/"$_username" UserShell /bin/zsh &&
		dscl . create /Users/"$_username" NFSHomeDirectory "/Users/$_username"
	} || return 1
}

function bulkuseraddhelp {
	echo "Usage: $0 [arguments]"
	echo ""
	echo "Arguments:"
	echo "  -h, --help                print help"
	echo "  -db, --database users.db  read data from selected SQLite database (default: users.db)"
	echo "  -na, --no-avatars         don't set user avatars"
	echo "  -lg, --list-groups        list groups"
	echo "  -lu, --list-users         list users"
	echo "  -anp, --allow-no-passwd   allow user login without password if not specified in database"
	echo "                            (not applicable for NetBSD and OpenBSD)"
}

LIST_GROUPS=0
LIST_USERS=0
SET_AVATAR=1
ALLOW_NO_PASSWD=0

for i in "$@"; do
	case $i in
		-db|--database)
			database="$2"
			shift 2
			;;
		-lg|--list-groups)
			LIST_GROUPS=1
			shift
			;;
		-lu|--list-users)
			LIST_USERS=1
			shift
			;;
		-na|--no-avatars)
			SET_AVATAR=0
			shift
			;;
		-anp|--allow-no-passwd)
			ALLOW_NO_PASSWD=1
			shift
			;;
		-h|--help)
			bulkuseraddhelp
			exit
			;;
		-*|--*)
			echo "Unknown option $i"
			exit 127
			;;
		*)
			;;
	esac
done

if [ ! -f "$database" ]; then echo "Database \"$database\" not found"; exit 2; fi
if [ ! "$($sqlitebin --version 2> /dev/null)" ]; then echo SQLite executable not found; exit 3; fi
if [ $kernel != "Darwin" ] && [ ! "$($imagickbin --version 2> /dev/null)" ]; then SET_AVATAR=0; fi

if [ $LIST_GROUPS == 1 ]; then listgroups; exit; fi
if [ $LIST_USERS == 1 ]; then listusers; exit; fi

tempdir="/tmp/bulkuseradd-$(date +%Y%m%d%H%M%S%N)"
mkdir -m 0700 "$tempdir"

addgroups
addusers

if [ $kernel == "Darwin" ]; then createhomedir -c; fi

rm -rf "$tempdir"
