/*
source code copied from http://web.archive.org/web/20141219050256/http://joco.name/2010/12/06/i-discovered-the-new-windows-user-tile-api/
Compile: %SystemRoot%\Microsoft.NET\Framework\v4.0.30319\csc.exe setUserAvatar.cs
Usage: setUserAvatar.exe UserName path\to\picture
Examples:
setUserAvatar.exe PieC avatar.png
setUserAvatar.exe test ..\Pictures\obrazek.png
setUserAvatar.exe Uzyszkodnik Z:\pic.bmp
*/

using System;
using System.Runtime.InteropServices;
 
namespace FejesJoco
{
    class Program
    {
        [DllImport("shell32.dll", EntryPoint = "#262", CharSet = CharSet.Unicode, PreserveSig = false)]
        public static extern void SetUserTile(string username, int whatever, string picpath);
 
        [STAThread]
        static void Main(string[] args)
        {
            SetUserTile(args[0], 0, args[1]);
        }
    }
}